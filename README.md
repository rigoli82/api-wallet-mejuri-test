# api-wallet-mejuri-test
This is the backend side of a challenge about a small CR~~U~~D application in ReactJS.

Contains: 

* A stripped down Ruby on Rails application to serve as an API backend.
* A standalone script that emulates some shameful credit card data verifier.
* Partial testing, I was not able to make too much progress here, since I've just gave my first steps in Ruby last week.
* A Private key decryption to receive the form POST from the FE.
* A one data-destructive-message method to validate the credit card with the verifier, to avoid sending real data and yet be able to validate the credit card information.

## Run the server

0. ```bundle install```
0. ```rails server```

## Run the card verifier endpoint (standalone)

```ruby card_verifier/index.rb -o $IP -p $PORT```

## Notes

The server is currently running in https://api-wallet-mejuri-test-mrigoli.c9users.io just in case. There shouldn't be much of a problem to make it work, but I wasn't able to test it in different environments.
I'm not sure how much this server is active, but just ping me and I'll check it if needed.

## Config

There are just two rules condigurable to communicate with the credit card verifier endpoint in the environments config, pretty straightforward.

## Final thoughts

I hope the idea for the credit card verificator endpoint was really simple, since I'm a bit ashamed of what I did there, I made it quickly just to be able to make a request from the ruby backend and to try that way of communication with the endpoint. There is absolutely nothing to see there. (...nothing!).

And again, here were so much more to polish here, but I hope to meet at least the minimum expectations of the challenge. It was a challenge for me at least, and I'm glad that I had the opportunity to learn a bit of Ruby on Rails.

Thanks!

- Marcos.