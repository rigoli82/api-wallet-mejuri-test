class Api::V1::CreditCardsController < Api::V1::BaseController
  include ApplicationHelper
  
  def index
    respond_with get_credit_cards
  end

  def create
    if (!validCard(card_params))
      render json: { success: false, message: "card_not_valid" }, :status => 403, :layout => false
      return
    end
    
    if (!CreditCard.create(card_params).valid?)
      render json: { success: false, message: "invalid_data" }, :status => 403, :layout => false
      return
    end
    
    render json: { success: true }
  end

  def destroy
    CreditCard.delete(params[:id])
    render json: { success: true }
  end

  private
  
  def get_credit_cards
    CreditCard.select('id, number, expires').map { |row| { id: row.id, trailingNumber: row.number[-4..-1] || row.number, expires: row.expires } }
  end

  def card_params
    row = params.require(:credit_card).permit(:number, :cvv, :expires)
    row[:number] = decrypt(row[:number])
    row[:cvv] = decrypt(row[:cvv])
    row[:expires] = decrypt(row[:expires])
    return row
  end
end