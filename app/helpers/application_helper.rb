require 'net/http'
require 'uri'
require 'digest'

module ApplicationHelper
  
  # Decrypts a string using the stored private key
  def decrypt(string)
    private_key_file = Rails.root + "config/rsa/rsa_1024_priv.pem";
    private_key = OpenSSL::PKey::RSA.new(File.read(private_key_file))
    return private_key.private_decrypt(Base64.decode64(string))
  end
  
  def validCard(data)
    secret = Rails.configuration.card_verifier_secret
    hash = Digest::SHA2.hexdigest("#{data[:number]}#{data[:cvv]}#{data[:expires]}#{secret}")
    uri = URI.parse("#{Rails.configuration.card_verifier_uri}/validate?hash=#{hash}")
    response = Net::HTTP.get_response uri
    return response.body == "true"
  end
end
