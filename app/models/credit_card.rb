class CreditCard < ActiveRecord::Base
  validates :number, presence: true, format: { with: /\A[0-9]{16}\z/ }
  validates :cvv, presence: true, format: { with: /\A[0-9]{3}\z/ }
  validates :expires, presence: true, format: { with: /\A(0[1-9]|1[0-2])\/20\d\d\z/ }
end
