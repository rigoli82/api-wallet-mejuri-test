require 'sinatra'
require 'digest'
set :port, 8081

secret = 'abc123'

# matches "GET /validate?hash=fubar"
get '/validate' do
  hash = params[:hash]
  cardList = [
    { number: '0123456789123456', cvv: 123, expires: '12/2018', balance: 5000 },
    { number: '0123456789654321', cvv: 321, expires: '12/2018', balance: 200 },
    { number: '1111111111111111', cvv: 111, expires: '11/2020', balance: 0 },
    { number: '2222222222222222', cvv: 222, expires: '12/2020', balance: 50 },
    { number: '3333333333333333', cvv: 333, expires: '09/2020', balance: 500 }
  ]
  card = cardList.select {|data| data[:balance] > 0 && Digest::SHA2.hexdigest("#{data[:number]}#{data[:cvv]}#{data[:expires]}#{secret}") == hash }
  #card = cardList.select {|data| "#{data[:number]}#{data[:cvv]}#{data[:expires]}#{secret}" == hash }
  
  body card.any?.to_s
  halt 200
end