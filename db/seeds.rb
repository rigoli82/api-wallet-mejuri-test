# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Creating some cards for testing
CreditCard.create!(number: "1234567890123456", cvv: "123", expires: "06/2020")
CreditCard.create!(number: "1234567890654321", cvv: "321", expires: "02/2022")