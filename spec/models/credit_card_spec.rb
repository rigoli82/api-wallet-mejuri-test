require 'rails_helper'

RSpec.describe CreditCard, type: :model do
  subject do
    described_class.new(
      number: '1234567890123456',
      cvv: '123',
      expires: '12/2048'
    )
  end
  
  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end
  
  it "is not valid without a number" do
    subject.number = nil
    expect(subject).to_not be_valid
  end
  it "is not valid without a cvv" do
    subject.cvv = nil
    expect(subject).to_not be_valid
  end
  
  it "is not valid without an expiration date" do
    subject.cvv = nil
    expect(subject).to_not be_valid
  end
end
